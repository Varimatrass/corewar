/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   boot.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/28 14:27:31 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/28 18:04:07 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BOOT_H
# define BOOT_H

# include "mv.h"

typedef struct	s_files
{
	char			*path;
	int				nb;
	struct s_files	*next;
}				t_files;

# define GLAD t_process **gladiator
# define MANA t_player **manager
# define AREN t_data *arena

void		boot(t_files *files, GLAD, MANA, AREN);
t_files		*parse(int argc, char **argv, t_data *arena);
void		t_fichier_add(t_files **fichier, char *path, int nb);
t_files		*t_fichier_new(char *path, int nb);
void		ft_bad_param(char *txt);

#endif /* ! BOOT_H */
