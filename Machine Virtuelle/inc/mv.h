/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mv.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/26 23:04:12 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/30 20:36:57 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MV_H
# define MV_H

#include "op2.h"

typedef char	bool;

#define true	1
#define false	0

typedef struct	s_op
{
	char	*name;
	int		nbparam;
	int		param[4];
	int		opcode;
	int		nb_cycles;
	char	*description;
	int		coding_byte;
	int		madness_byte;
}				t_op;

typedef struct	s_inst
{
	struct s_op		ref;
	int				lag;
}				t_inst;


typedef struct	s_process
{
	char				registre[REG_NUMBER][REG_SIZE];
	int					pc;
	bool				carry;
	bool				live;
	int					num_player;
	struct s_inst		*action_hope;
	struct s_process	*next;
	struct s_process	*previous;
}				t_process;

typedef struct	s_player
{
	int				number;
	char			*name_champ;
	char			*comment_champ;
	struct s_player	*next;
}				t_player;

typedef struct	s_data
{
	char	core[MEM_SIZE];
	int		dump;
	int		last_player_live;
	int		cycle;
}				t_data;

#endif /* ! MV_H */
