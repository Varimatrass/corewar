/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/26 23:09:16 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/29 21:27:08 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mv.h"
#include "boot.h"


int		main(int argc, char **argv)
{
	t_files		*files;
	t_process	*gladiator;
	t_player	*manager;
	t_data		arena;

	files = parse(argc, argv, &arena);
	boot(files, &gladiator, &manager, &arena);
	fight(gladiator, manager, arena);

	return (0);
}
