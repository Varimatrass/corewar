/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fight.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kelickel <kelickel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/28 15:00:48 by kelickel          #+#    #+#             */
/*   Updated: 2014/01/31 00:28:50 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "boot.h"
#include "mv.h"
#include "op.h"
#include "op2.h"

void	last_player(t_data *one, t_player *two)
{
	int	a;

	a = one->last_player_live;
	while (two->next != 0)
	{
		two = two->next;
		if (a == two->number)
			ft_printf("Player %d(%s) win !", a, two->name_champ);
	}
}

bool	check(t_process *one)
{
	while (one->next != 0)
	{
		if (one->live == true)
			return (1);
		one = one->next;
	}
	return (0);
}

void	ft_fight(t_process *one, t_player *two, t_data *three)
{
	int	cycle;
	int	nb_check;

	nb_check = 0;
	while (CYCLE_TO_DIE != 0)
	{
		cycle = 0;
		While (cycle != CYCLE_TO_DIE)
			cycle++;
		if (check(one) == 0)
			last_player(three, two);
		nb_check++;
		if (nb_check == MAX_CHECKS)
			CYCLE_TO_DIE = CYCLE_TO_DIE - CYCLE_DELTA;
	}
	last_player(three, two)
}


void	fight(t_process *gladiator, t_player *manager, t_data arena)
{
	while (gladiator)
	{
		check_begin(&gladiator, manager, &arena);
		roll_piety_roll(&gladiator, manager, &arena);
		check_end(&gladiator, manager, &arena);
	}
	and_victory_is_to(manager, arena);
}

void	roll_piety_roll(t_process **gladiator, t_player *manager, t_data *arena)
{
	t_process	actif;

	actif = *gladiator;
	while (actif)
	{
		if (actif->action_hope->lag == 0)
			headache(actif); // activation de la comp
		else
			actif->action_hope->lag--;
		actif = actif->next;
	}
}

void	headache(t_process *actif, GLAD, MANA, AREN)
{
	int	code;

	if (actif->action_hope->ref != NULL)
	{
		// execution de l'action
	}
	else
	{
		code = ;//lecture de l'action suivante
	}
}

void	check_begin()
{
	// ?
}

void	check_end()
{
	// verif de cycle to die
	//		assasina a coup de batte en mouse
	// verif de decrementation
}
