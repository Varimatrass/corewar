/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   boot.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/26 23:35:03 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/29 21:21:11 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include "libft.h"
#include "boot.h"
#include "mv.h"

t_player	*t_player_new(void);
char		*ft_getstrwar(int fd, int nb);
int			ft_getnbwar(int fd, int nb);
void		ft_code_leech(int fd, char *core, int size, int start);
void		ft_create_process(GLAD, int start, int p_number);
void		ft_error(char *txt, int res);
int			ft_files_count(t_files *files);
t_process	*t_process_new();
void		ft_fill_register(t_process *gladiator, int num_reg, void *value);

int			ft_files_count(t_files *files)
{
	int	i;

	i = 0;
	while (files)
	{
		i++;
		files = files->next;
	}
	return (i);
}

void		ft_error(char *txt, int res)
{
	ft_putendl_fd(txt, 2);
	exit(res);
}

void		ft_code_leech(int fd, char *core, int size, int start)
{
	char	buff[1];

	if (start + size > MEM_SIZE)
		ft_error("Alert! supply exceeds demand! restart factories smurf", -1);
	while (size > 0 && read(fd, buff, 1))
	{
		core[start] = buff[0];
		start++;
		size--;
	}
	if (size > 0)
		ft_error("Dirty megalo ! Lying about your weight you worth a TIG!", -1);
}


void		boot(t_files *files, GLAD, MANA, AREN)
{
	int			size;
	int			fd;
	int			start;
	int			i;
	int			nb_champ;
	t_player	*tmp;

	*manager = t_player_new();
	tmp = *manager;
	i = 0;
	nb_champ = ft_files_count(files);
	while (files)
	{
		if ((fd = open(files->path, O_RDONLY)) < 0)
		{
			perror(files->path);
			exit(-1);
		}
		tmp->number = files->nb;
		if (ft_getnbwar(fd, 4) != COREWAR_EXEC_MAGIC)
			ft_error("Magical power is empty", -1);
		tmp->name_champ = ft_getstrwar(fd, PROG_NAME_LENGTH + 1);
		if ((size = ft_getnbwar(fd, 4)) > CHAMP_MAX_SIZE)
			ft_error("Gladiator soooo biiiig", -1);
		tmp->comment_champ = ft_getstrwar(fd, COMMENT_LENGTH + 1);
		start = (MEM_SIZE / nb_champ) * i;
		ft_code_leech(fd, arena->core, size, start);
		ft_create_process(gladiator, start, tmp->number);
		files = files->next;
		if (files)
			tmp->next = t_player_new();
		tmp = tmp->next;
		i++;
	}
}

void	ft_create_process(GLAD, int start, int p_number)
{
	t_process	*res;
	t_process	*tmp;

	res = t_process_new();
	res->pc = start;
	res->nb_player = p_number;
	ft_fill_register(res, 1, &p_number);
	if (*gladiator == NULL)
	{
		*gladiator = res;
		return ;
	}
	tmp = *gladiator;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = res;
	res->previous = tmp;
}

t_process	*t_process_new(void)
{
	t_process	*res;

	res = malloc(sizeof(t_process));
	res->pc = 0;
	res->carry = false;
	res->live = false;
	res->nb_player = -1;
	res->next = NULL;
	res->previous = NULL;
	return (res);
}

t_player	*t_player_new(void)
{
	t_player	*res;

	res = malloc(sizeof(t_player));
	res->number = -1;
	res->name_champ = NULL;
	res->comment_champ = NULL;
	res->next = NULL;
	return (res);
}

void		ft_fill_register(t_process *gladiator, int num_reg, void *value)
{
	char	tmp;
	int		i;

	i = 0;
	if (num_reg > REG_NUMBER)
		return ;
	while (i < REG_SIZE)
	{
		if (REG_SIZE - i > (int) sizeof(*value))
			tmp = 0;
		else
			tmp = *((char *)value) >> ((REG_SIZE - i - 1) * 8);
		gladiator->registre[num_reg][i] = tmp;
		i++;
	}
}

int			ft_getnbwar(int fd, int nb)
{
	unsigned char	*txt;
	unsigned int	res;
	int				i;

	if (nb < 1 || nb > 4)
		ft_error("The integer have a fixed size baka !", -1);
	txt = (unsigned char *) ft_getstrwar(fd, nb);
	res = txt[0];
	i = 1;
	while (i < nb)
	{
		res = res << 8;
		res += txt[i];
		i++;
	}
	return (res);
}

char		*ft_getstrwar(int fd, int nb)
{
	char	*buff;

	if (nb < 1)
		return (NULL);
	buff = ft_strnew(nb);
	if (read(fd, buff, nb) != nb)
		ft_error("An early end to a stillborn champion", -1);
	return (buff);
}

t_files		*parse(int argc, char **argv, t_data *arena)
{
	int		i;
	int		nb;
	t_files	*fichier;

	fichier = NULL;
	if (argc < 2)
		ft_bad_param(NULL);
	i = 1;
	if (ft_strcmp(argv[1], "-dump") == 0)
	{
		if (argc < 3)
			ft_bad_param(NULL);
		arena->dump = ft_atoi(argv[2]);
		i = 3;
	}
	while (i < argc)
	{
		nb = -1;
		if (ft_strcmp(argv[i], "-n") == 0)
		{
			if (argc <= (i + 2))
				ft_bad_param(NULL);
			i++;
			nb = ft_atoi(argv[i]);
			i++;
		}
		if (ft_strrncmp(argv[i], ".cor", 4) != 0)
			ft_bad_param(NULL);
		t_fichier_add(&fichier, argv[i], nb);
		i++;
	}
	return (fichier);
}

void		t_fichier_add(t_files **fichier, char *path, int nb)
{
	t_files	*tmp;
	int		i;

	i = 1;
	if (!fichier)
		return ;;
	if (*fichier == NULL)
	{
		*fichier = t_fichier_new(path, nb);
		if (nb == -1)
			(*fichier)->nb = 1;
		return ;
	}
	tmp = *fichier;
	while (tmp->next != NULL)
	{
		if (tmp->nb == nb)
			ft_bad_param("Erreur : deux numero identique");
		if (tmp->nb >= i)
			i = tmp->nb + 1;
		tmp = tmp->next;
	}
	if (tmp->nb == nb)
		ft_bad_param("Erreur : deux numero identique");
	if (tmp->nb >= i)
			i = tmp->nb + 1;
	if (nb == -1)
		nb = i;
	tmp->next = t_fichier_new(path, nb);
}

t_files		*t_fichier_new(char *path, int nb)
{
	t_files	*res;

	res = malloc(sizeof(t_files));
	res->path = path;
	res->nb = nb;
	res->next = NULL;
	return (res);
}

void		ft_bad_param(char *txt)
{
	if (!txt)
		txt = "corewar [-dump nbr_cycles] [[-n number] champion1.cor] ...";
	ft_putendl(txt);
	exit(-1);
}
