# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/01/03 16:01:28 by bgauci            #+#    #+#              #
#    Updated: 2014/01/23 15:16:12 by bgauci           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME_ASM = asm
DIR_ASM = Assembleur

NAME_MV = corewar
DIR_MV = Machine\ Virtuelle

LIB = libft
LIBDIR = ./$(LIB)/

ifeq ($(W),)
FLAG =	W=$(W)
endif

RMF = rm -f

all: $(NAME_ASM) $(NAME_MV) $(NAME_CHAMP)

$(NAME_ASM): libft
	make -C Assembleur $(FLAG)
	mv $(DIR_ASM)/$(NAME_ASM) .

$(NAME_MV): libft
	make -C Machine\ Virtuelle $(FLAG)
	mv $(DIR+_MV)/$(NAME_MV) .

clear: clean
clean:
	make -C Assembleur clean $(FLAG)
	make -C Machine\ Virtuelle clean $(FLAG)
	make -C libft clean $(FLAG)

fclean: clean
	make -C Assembleur fclean $(FLAG)
	make -C Machine\ Virtuelle fclean $(FLAG)
	make -C libft fclean $(FLAG)
	$(RMF) $(NAME_CHAMP) $(NAME_ASM) $(NAME_MV)

libft:
	make -C $(LIBDIR)

re: fclean all

depend:
	make -C Assembleur depend $(FLAG)
	make -C Machine\ Virtuelle depend $(FLAG)

.PHONY: all re clean fclean depend libft
