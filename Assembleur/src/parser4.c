/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser4.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/21 18:18:19 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/21 18:18:21 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "op2.h"
#include "token.h"
#include "libft.h"
#include "parser.h"
#include <string.h>
#include <stdlib.h>

t_token	*ligne2(char *line, int *i)
{
	t_token	*res;
	t_token	*tmp;
	char	*str;
	int		j;

	if ((res = n_cmd(line, i)) != NULL)
		return (res);
	else if ((res = c_cmd(line, i)) != NULL)
		return (res);
	else
	{
		j = *i;
		if ((str = label(line, &j)) != NULL && line[j] == LABEL_CHAR)
		{
			tmp = ft_token_new(str, LABEL);
			ft_token_addend(&res, tmp);
			*i = j + 1;
		}
		while (line[*i] == ' ' || line[*i] == '\t')
			(*i)++;
		if ((tmp = inst(line, i)) != NULL)
			ft_token_addend(&res, tmp);
	}
	return (res);
}
