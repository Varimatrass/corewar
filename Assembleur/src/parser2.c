/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/21 18:18:07 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/23 14:41:20 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "op2.h"
#include "token.h"
#include "libft.h"
#include "parser.h"
#include <string.h>
#include <stdlib.h>


t_token	*n_cmd(char *line, int *i)
{
	char	*str;
	char	*NCS;
	int		len_NCS;

	NCS = NAME_CMD_STRING;
	len_NCS = ft_strlen(NCS);
	if (ft_strncmp(NCS, line + *i, len_NCS) != 0)
		return (NULL);
	*i += len_NCS;
	while (line[*i] == ' ' || line[*i] == '\t')
		(*i)++;
	str = NULL;
	if (line[*i] != '"')
		critical_error("n_cmd", line, i);
	(*i)++;
	while (line[*i] != '"')
	{
		ft_strnjcat(&str, line + *i, 1);
		(*i)++;
		if (line[*i] == '\0')
			critical_error("n_cmd", line, i);
	}
	(*i)++;
	return (ft_token_new(str, N_CMD_STRING));
}

char	*nums(char *line, int *i)
{
	char	*str;
	char	*tmp;

	if (line[*i] == '-')
	{
		(*i)++;
		if ((str = num(line, i)) == NULL)
		{
			(*i)--;
			return (NULL);
		}
		tmp = str;
		str = ft_strjoin("-", tmp);
		free(tmp);
	}
	else if ((str = num(line, i)) == NULL)
		return (NULL);
	while ((tmp = num(line, i)) != NULL)
	{
		ft_strjcat(&str, tmp);
		free(tmp);
	}
	return (str);
}

t_token	*name(char *line, int *i)
{
	t_token	*res;
	int		j;
	int		op_name_len;

	j = 0;
	while (op_tab[j].name != NULL)
	{
		op_name_len = ft_strlen(op_tab[j].name);
		if (line[*i + op_name_len] == ' ' || line[*i + op_name_len] == '\t')
		{
			if (!ft_strncmp(line + *i, op_tab[j].name, op_name_len))
			{
				res = ft_token_new(op_tab[j].name, INSTRUCTION);
				*i += op_name_len;
				return (res);
			}
		}
		j++;
	}
	return (NULL);
}

t_token	*param_reg(char *line, int *i)
{
	t_token	*res;
	char	*str;

	if (line[*i] != 'r')
		return (NULL);
	(*i)++;
	if ((str = nums(line, i)) == NULL)
		critical_error("param_reg", line, i);
	res = ft_token_new(ft_strjoin("r", str), PARAM_REG);
	free(str);
	return (res);
}

t_token	*param_ind(char *line, int *i)
{
	t_token	*res;
	char	*str;

	if (line[*i] != LABEL_CHAR)
	{
		if ((str = nums(line, i)) == NULL)
			return (NULL);
		res = ft_token_new(str, PARAM_IND_NUMS);
		free(str);
		return (res);
	}
	(*i)++;
	if ((str = label(line, i)) == NULL)
		critical_error("param_ind", line, i);
	res = ft_token_new(str, PARAM_IND_LABEL);
	free(str);
	return (res);
}
