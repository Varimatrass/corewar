/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/21 18:27:37 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/21 18:37:37 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "op2.h"
#include "check.h"
#include "libft.h"

int		error(char *s, t_token *code, int i);
int		check(t_token *code);
int		check_label(t_token *code);
int		check_inst(t_token **code);

int		error(char *s, t_token *code, int i)
{
	ft_putstr(s);
	ft_putstr(" : ");
	ft_putendl(code->value);
	return (i);
}

int		check(t_token *code)
{
	if (code->type != N_CMD_STRING)
		return (error(".name attendu", code, 0));
	code = code->next;
	if (code->type != C_CMD_STRING)
		return (error(".comment attendu", code, 0));
	while ((code = code->next) != NULL)
	{
		if (code->type == N_CMD_STRING)
			return (error(".name invalide", code, 0));
		else if (code->type == C_CMD_STRING)
			return (error(".comment invalide", code, 0));
		else if (code->type == LABEL)
		{
			if (!check_label(code))
				return (error("label invalide", code, 0));
		}
		else if (code->type == INSTRUCTION)
		{
			if (!check_inst(&code))
				return (error("instruction invalide", code, 0));
		}
		else
			return (error("code invalide", code, 0));
	}
	return (1);
}

int		check_label(t_token *code)
{
	while (code->type == LABEL)
	{
		if (code->next == NULL)
			return (1);
		code = code->next;
	}
	if (code->type != INSTRUCTION)
		return (error("label non suivis par une instruction", code, 0));
	return (1);
}

int		check_inst(t_token **code)
{
	int				i;
	int				j;
	enum e_token	t;

	i = 0;
	while (ft_strcmp(op_tab[i].name, (*code)->value) != 0)
		i++;
	j = 0;
	while (op_tab[i].nbparam > j)
	{
		*code = (*code)->next;
		t = (*code)->type;
		if (t == PARAM_REG && (op_tab[i].param[j] & T_REG) != T_REG)
			return (error("parametre registre non attendu", *code, 0));
		if ((t == PARAM_IND_LABEL || t == PARAM_IND_NUMS)
			&& (op_tab[i].param[j] & T_IND) != T_IND)
			return (error("parametre indirect non attendu", *code, 0));
		if ((t == PARAM_DIR_LABEL || t == PARAM_DIR_NUMS)
			&& (op_tab[i].param[j] & T_DIR) != T_DIR)
			return (error("parametre direct non attendu", *code, 0));
		j++;
	}
	return (1);
}
