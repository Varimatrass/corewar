/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/22 15:33:53 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/23 15:03:55 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "op2.h"
#include "write.h"
#include "hum.h"
#include "check.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void	trad(t_token *code, int fd)
{
	int			i;
	t_tree_AVL	*tab;

	write_header(&code, ft_count_size(code, &tab), fd);
	i = 0;
	while (code)
	{
		if (code->type == INSTRUCTION)
			write_int(&code, tab, &i, fd);
		else
			code = code->next;
	}
}

int		write_header(t_token **code, int prog_len, int fd)
{
	t_token	*tmp;

	ft_putwarnbr(COREWAR_EXEC_MAGIC, sizeof(unsigned int), fd);
	tmp = *code;
	while (tmp->type != N_CMD_STRING)
	{
		tmp = tmp->next;
		if (!tmp)
			return (error("N_CMD_STRING", tmp, 0));
	}
	ft_putwarstr(tmp->value, PROG_NAME_LENGTH + 1, fd);
	ft_putwarnbr(prog_len, sizeof(unsigned int), fd);
	tmp = *code;
	while (tmp->type != C_CMD_STRING)
	{
		tmp = tmp->next;
		if (!tmp)
			return (error("C_CMD_STRING", tmp, 0));
	}
	ft_putwarstr(tmp->value, COMMENT_LENGTH + 1, fd);
	return (0);
}

void	write_int(t_token **code, t_tree_AVL *tab, int *i, int fd)
{
	int	op;
	int	i_fd[2];

	op = 0;
	while (ft_strcmp(op_tab[op].name, (*code)->value))
		op++;
	ft_putwarnbr(op_tab[op].opcode, 1, fd);
	*code = (*code)->next;
	i_fd[0] = *i;
	i_fd[1] = fd;
	*i += write_param(op_tab[op], *code, tab, i_fd);
	(*i)++;
}


int		write_opcode(t_op op, t_token *code, int fd)
{
	int		j;
	int		opcode;

	j = 0;
	opcode = 0;
	while (j < op.nbparam)
	{
		if (code->type == PARAM_REG)
			opcode += REG_CODE;
		else if (code->type == PARAM_DIR_LABEL || code->type == PARAM_DIR_NUMS)
			opcode += DIR_CODE;
		else if (code->type == PARAM_IND_LABEL || code->type == PARAM_IND_NUMS)
			opcode += IND_CODE;
		opcode = opcode << 2;
		code = code->next;
		j++;
	}
	while (j < 3)
	{
		opcode = opcode << 2;
		j++;
	}
	ft_putwarnbr(opcode, 1, fd);
	return (1);
}
