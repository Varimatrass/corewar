/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hum.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/21 20:41:42 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/25 15:50:29 by mde-jesu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "op2.h"
#include "hum.h"
#include <unistd.h>


int		ft_count_size(t_token *code, t_tree_AVL **tab)
{
	int	i;

	i = 0;
	*tab = NULL;
	while (code)
	{
		if (code->type == INSTRUCTION)
			incemental_int(&code, &i);
		else
		{
			if (code->type == LABEL)
				ft_tree_add(tab, ft_tree_new(code->value, ft_itoa(i)));
			code = code->next;
		}
	}
	return (i);
}

void	ft_putwarstr(char *str, int i, int fd)
{
	int	j;

	j = 0;
	while (j < i)
	{
		ft_putchar_fd(str[j], fd);
		if (str[j] != '\0')
			j++;
		else
			i--;
	}
}

void	ft_putwarnbr(int nb, int i, int fd)
{
	char	o;

	if (i == 4)
	{
		o = nb >> 24;
		write(fd, &o, 1);
		o = nb >> 16;
		write(fd, &o, 1);
		o = nb >> 8;
		write(fd, &o, 1);
		write(fd, &nb, 1);
	}
	else if (i == 2)
	{
		o = nb >> 8;
		write(fd, &o, 1);
		write(fd, &nb, 1);
	}
	else if (i == 1)
		write(fd, &nb, 1);
}

void	incemental_int(t_token **code, int *i)
{
	int	f;

	f = 0;
	while (ft_strcmp(op_tab[f].name, (*code)->value) != 0)
		f++;
	*i += ((op_tab[f].coding_byte) ? 1 : 0);
	*i += 1;
	while (1)
	{
		if ((*code = (*code)->next) == NULL)
			return ;
		if ((*code)->type == PARAM_REG)
			*i += 1;
		else if ((*code)->type == PARAM_DIR_LABEL)
			*i += ((op_tab[f].madness_byte) ? 2 : 4);
		else if ((*code)->type == PARAM_DIR_NUMS)
			*i += ((op_tab[f].madness_byte) ? 2 : 4);
		else if ((*code)->type == PARAM_IND_LABEL)
			*i += 2;
		else if ((*code)->type == PARAM_IND_NUMS)
			*i += 2;
		else
			return ;
	}
}
