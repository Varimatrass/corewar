/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/03 18:19:09 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/23 14:41:51 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "op2.h"
#include "token.h"
#include "libft.h"
#include "parser.h"

void	critical_error(char *str, char *line, int *i)
{
	ft_putstr("critical error in parser : ");
	ft_putendl(str);
	ft_putstr("line : ");
	ft_putendl(line);
	ft_putstr("i = ");
	ft_putnbr(*i);
	ft_putstr("\n");
	exit(0);
}

t_token		*parser(int fd)
{
	return (prog(fd));
}

char	*num(char *line, int *i)
{
	char	*res;

	if (ft_isdigit(line[*i]) == 0)
		return (NULL);
	res = malloc(sizeof(char) * 2);
	res[0] = line[*i];
	res[1] = '\0';
	(*i)++;
	return (res);
}

char	*label(char *line, int *i)
{
	char	*str;

	if (ft_strchr(LABEL_CHARS, line[*i]) == NULL)
		return (NULL);
	str = ft_strnjoin(NULL, line + *i, 1);
	(*i)++;
	while (ft_strchr(LABEL_CHARS, line[*i]) != NULL)
	{
		ft_strnjcat(&str, line + *i, 1);
		(*i)++;
	}
	return (str);
}

t_token	*c_cmd(char *line, int *i)
{
	char	*str;
	char	*CCS;
	int		len_CCS;

	CCS = COMMENT_CMD_STRING;
	len_CCS = ft_strlen(CCS);
	if (ft_strncmp(CCS, line + *i, len_CCS) != 0)
		return (NULL);
	*i += len_CCS;
	while (line[*i] == ' ' || line[*i] == '\t')
		(*i)++;
	str = NULL;
	if (line[*i] != '"')
		critical_error("c_cmd", line, i);
	(*i)++;
	while (line[*i] != '"')
	{
		ft_strnjcat(&str, line + *i, 1);
		(*i)++;
		if (line[*i] == '\0')
			critical_error("c_cmd", line, i);
	}
	(*i)++;
	return (ft_token_new(str, C_CMD_STRING));
}
