/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/23 14:42:03 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/25 15:49:32 by mde-jesu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "op2.h"
#include "write.h"
#include "hum.h"
#include "check.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int		write_param(t_op op, t_token *code, t_tree_AVL *tab, int *i_fd)
{
	int	j;
	int	label;

	j = 0;
	if (op.coding_byte == 1)
		j = write_opcode(op, code, i_fd[1]);
	while (code)
	{
		label = calc_label(i_fd[0], ft_tree_chr(tab, code->value));
		if (code->type == PARAM_REG)
			j += write_param_reg((code->value) + 1, i_fd[1]);
		else if (code->type == PARAM_DIR_LABEL || code->type == PARAM_DIR_NUMS)
			j += write_param_dir(code, i_fd[1], label, op.madness_byte);
		else if (code->type == PARAM_IND_LABEL || code->type == PARAM_IND_NUMS)
			j += write_param_ind(code, i_fd[1], label);
		else
			return (j);
		code = code->next;
	}
	return (j);
}

int		write_param_dir(t_token *code, int fd, int label, int au_fous)
{
	int	nb;
	int	octet;

	if (code->type == PARAM_DIR_LABEL)
		nb = label;
	else
		nb = ft_atoi(code->value);
	octet = ((au_fous) ? 2 : 4);
	ft_putwarnbr(nb, octet, fd);
	return (octet);
}

int		write_param_ind(t_token *code, int fd, int label)
{
	if (code->type == PARAM_IND_LABEL)
		ft_putwarnbr(label, 2, fd);
	else
		ft_putwarnbr(ft_atoi(code->value), 2, fd);
	return (2);
}

int		write_param_reg(char *txt, int fd)
{
	ft_putwarnbr(ft_atoi(txt), 1, fd);
	return (1);
}

int		calc_label(int i, char *label_value)
{
	int	pos;
	int	neg;

	pos = ft_atoi(label_value);
	if (i <= pos)
		return (pos - i);
	neg = 0xFFFFFFFF;
	neg -= (i - pos - 1);
	return (neg);
}
