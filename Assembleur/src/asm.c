/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/03 18:19:23 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/23 15:02:51 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "op2.h"
#include "asm.h"
#include "hum.h"
#include "write.h"
#include "parser.h"
#include "check.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int		*get_param(int argc, char **argv)
{
	char	*file;
	int		*fd;

	fd = malloc(sizeof(int) * 2);
	if (argc != 2)
		ft_error("usage : asm fichier.s\n", 0);
	if (ft_strcmp(argv[1] + ft_strlen(argv[1]) - 2, ".s") != 0)
		ft_error("usage : asm fichier.s\n", 0);
	if ((fd[0] = open(argv[1], O_RDONLY)) <= 0)
	{
		ft_putstr_fd(argv[1], 2);
		perror(" : open fail ");
		exit(-1);
	}
	ft_putstr(argv[1]);
	argv[1][ft_strlen(argv[1]) - 2] = '\0';
	file = ft_strjoin(argv[1], ".cor");
	if ((fd[1] = open(file, O_WRONLY | O_CREAT | O_TRUNC, 0644)) <= 0)
	{
		ft_putstr_fd(file, 2);
		perror(" : open fail ");
		exit(-1);
	}
	return (fd);
}

void	ft_error(char *txt, int res)
{
	ft_putstr_fd(txt, 2);
	exit(res);
}

void	ft_print_message(t_token *code);

int		main(int argc, char **argv)
{
	int		*fd;
	t_token	*code;

	ft_putstr("Assembling ");
	fd = get_param(argc, argv);
	ft_putendl(":");
	code = parser(fd[0]);
	ft_print_message(code);
	if (!check(code))
		return (-1);
	trad(code, fd[1]);
	return (0);
}

void	ft_print_message(t_token *code)
{
	char	*name;
	char	*com;

	while (code)
	{
		if (code->type == N_CMD_STRING)
			name = code->value;
		else if (code->type == C_CMD_STRING)
			com = code->value;
		code = code->next;
	}
	ft_putstr("\t");
	ft_putendl(name);
	ft_putstr("\t");
	ft_putendl(com);
}
