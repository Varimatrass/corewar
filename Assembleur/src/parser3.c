/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser3.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/21 18:18:12 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/23 14:40:53 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "op2.h"
#include "token.h"
#include "libft.h"
#include "parser.h"
#include <string.h>
#include <stdlib.h>


t_token	*param_dir(char *line, int *i)
{
	t_token	*res;
	char	*str;

	if (line[*i] != DIRECT_CHAR)
		return (NULL);
	(*i)++;
	if (line[*i] != LABEL_CHAR)
	{
		if ((str = nums(line, i)) == NULL)
			critical_error("param_dir", line, i);
		res = ft_token_new(str, PARAM_DIR_NUMS);
		free(str);
		return (res);
	}
	(*i)++;
	if ((str = label(line, i)) == NULL)
		critical_error("param_dir", line, i);
	res = ft_token_new(str, PARAM_DIR_LABEL);
	free(str);
	return (res);
}

t_token	*prog(int fd)
{
	int		i;
	char	*line;
	t_token	*res;
	t_token	*tmp;

	line = NULL;
	res = NULL;
	while (gnl(fd, &line) != 0)
	{
		i = 0;
		if ((tmp = ligne(line, &i)) != NULL)
			ft_token_addend(&res, tmp);
		free(line);
		line = NULL;
	}
	return (res);
}

t_token	*inst(char *line, int *i)
{
	t_token	*res;
	t_token	*tmp;

	if ((res = name(line, i)) == NULL)
		return (NULL);
	while (line[*i] == ' ' || line[*i] == '\t')
		(*i)++;
	if ((tmp = param(line, i)) == NULL)
		critical_error("inst", line, i);
	ft_token_addend(&res, tmp);
	while (line[*i] == SEPARATOR_CHAR)
	{
		(*i)++;
		while (line[*i] == ' ' || line[*i] == '\t')
			(*i)++;
		if ((tmp = param(line, i)) == NULL)
			critical_error("inst", line, i);
		ft_token_addend(&res, tmp);
	}
	return (res);
}

t_token	*param(char *line, int *i)
{
	t_token	*res;

	if ((res = param_dir(line, i)) != NULL)
		return (res);
	else if ((res = param_ind(line, i)) != NULL)
		return (res);
	else if ((res = param_reg(line, i)) != NULL)
		return (res);
	else
		return (NULL);
}

t_token	*ligne(char *line, int *i)
{
	t_token	*res;

	res = ligne2(line, i);
	while (line[*i] == ' ' || line[*i] == '\t')
		(*i)++;
	if (line[*i] == COMMENT_CHAR || line[*i] == '\0')
		return (res);
	critical_error("ligne", line, i);
	return (NULL);
}
