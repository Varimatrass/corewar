/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 12:45:22 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/07 19:13:38 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "token.h"
#include "libft.h"
#include <stdlib.h>

t_token	*ft_token_new(char *value, enum e_token type)
{
	t_token	*res;

	res = malloc(sizeof(t_token));
	res->value = ft_strdup(value);
	res->type = type;
	res->next = NULL;
	return (res);
}

void	ft_token_addend(t_token **root, t_token *sheet)
{
	t_token	*tmp;

	if (!root)
		return ;
	if (*root == NULL)
	{
		*root = sheet;
		return ;
	}
	tmp = *root;
	while (tmp->next != NULL)
		tmp = tmp->next;
	tmp->next = sheet;
}
