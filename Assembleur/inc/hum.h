/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hum.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/22 15:26:35 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/22 15:28:30 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUM_H
# define HUM_H

# include "token.h"
# include "libft.h"

int		ft_count_size(t_token *code, t_tree_AVL **tab);
void	ft_putwarstr(char *str, int i, int fd);
void	ft_putwarnbr(int nb, int i, int fd);
void	incemental_int(t_token **code, int *i);

#endif /* !HUM_H */
