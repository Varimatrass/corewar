/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/05 17:01:21 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/22 15:33:32 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASM_H
# define ASM_H

# include "token.h"
# include "libft.h"

void	ft_error(char *txt, int res);
int		*get_param(int argc, char **argv);

#endif /* !ASM_H */
