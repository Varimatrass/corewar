/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 12:03:29 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/23 14:40:11 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

# include "token.h"

void		critical_error(char *str, char *line, int *i);
t_token		*parser(int fd);
char		*num(char *line, int *i);
char		*label(char *line, int *i);
t_token		*c_cmd(char *line, int *i);
t_token		*n_cmd(char *line, int *i);
char		*nums(char *line, int *i);
t_token		*name(char *line, int *i);
t_token		*param_reg(char *line, int *i);
t_token		*param_ind(char *line, int *i);
t_token		*param_dir(char *line, int *i);
t_token		*prog(int fd);
t_token		*inst(char *line, int *i);
t_token		*param(char *line, int *i);
t_token		*ligne(char *line, int *i);
t_token		*ligne2(char *line, int *i);

#endif
