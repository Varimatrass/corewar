/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/22 15:32:05 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/23 14:27:36 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WRITE_H
# define WRITE_H

# include "token.h"
# include "libft.h"

void	trad(t_token *code, int fd);
int		write_header(t_token **code, int prog_len, int fd);
int		calc_label(int i, char *label_value);
void	write_int(t_token **tmp, t_tree_AVL *tab, int *i, int fd);
int		write_param(t_op op, t_token *code, t_tree_AVL *tab, int *i_fd);
int		write_opcode(t_op op, t_token *code, int fd);
int		write_param_dir(t_token *code, int fd, int label, int au_fous);
int		write_param_ind(t_token *code, int fd, int label);
int		write_param_reg(char *txt, int fd);

#endif /* !WRITE_H */
