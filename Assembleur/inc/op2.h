/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op2.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 14:49:18 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/25 15:44:09 by mde-jesu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OP2_H
# define OP2_H

# include "op.h"

typedef struct	s_op
{
	char	*name;
	int		nbparam;
	int		param[4];
	int		opcode;
	int		nb_cycles;
	char	*description;
	int		coding_byte;
	int		madness_byte;
}				t_op;

extern t_op op_tab[];

#endif /* !OP2_H */
