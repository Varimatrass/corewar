/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/21 18:30:09 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/21 18:30:53 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECK_H
# define CHECK_H

# include "token.h"

int		error(char *s, t_token *code, int i);
int		check(t_token *code);
int		check_label(t_token *code);
int		check_inst(t_token **code);

#endif /* !CHECK_H */
