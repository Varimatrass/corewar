/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgauci <bgauci@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/05 15:10:13 by bgauci            #+#    #+#             */
/*   Updated: 2014/01/23 13:58:49 by bgauci           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOKEN_H
# define TOKEN_H

typedef enum	e_token
{
	INSTRUCTION,
	PARAM_DIR_LABEL,
	PARAM_DIR_NUMS,
	PARAM_REG,
	PARAM_IND_LABEL,
	PARAM_IND_NUMS,
	SEPARATOR,
	LABEL,
	C_CMD_STRING,
	N_CMD_STRING
}				t_etoken;

typedef struct	s_token
{
	char			*value;
	enum e_token	type;
	struct s_token	*next;
}				t_token;

t_token	*ft_token_new(char *value, enum e_token type);
void	ft_token_addend(t_token **root, t_token *sheet);

#endif /* !TOKEN_H */
